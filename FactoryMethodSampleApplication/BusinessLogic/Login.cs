using System;

namespace BusinessLogic
{
    public class Login
    {
        public static ILogin attemptLogin(string username, string password)
        {
            ILogin objectType = null;
            if (username != null && password != null)
            {
                objectType = new User(username, password);
            }

            return objectType;
        }
      

    }
}
