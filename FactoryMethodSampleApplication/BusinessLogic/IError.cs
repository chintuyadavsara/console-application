using System;

namespace BusinessLogic
{
    public interface IError
    {
        void showError();
        
    }
}
