﻿using System;

namespace BusinessLogic
{
    public interface IAuthentication
    {
        bool AttemptLogin(string username, string password);
    }
}
