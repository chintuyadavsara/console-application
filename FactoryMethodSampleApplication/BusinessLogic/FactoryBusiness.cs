using System;

namespace BusinessLogic
{
    public static class FactoryBusiness
    {
        public static IAuthentication Authenticate()
        {
            return new Authentication();
        }
        public static IStudentBussiness GetStudentRepo()
        {
            return new StudentBussiness();
        }


    }
}
