﻿using System;

namespace BusinessLogic
{
    public interface ILogin
    {
        bool AttemptLogin();
        string getUserName();

        string getUserId();
        
    }
}
