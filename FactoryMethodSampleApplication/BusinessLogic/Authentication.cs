using System;
using DataAccessLogic;
using DomainModal;
namespace BusinessLogic
{
    public class Authentication : IAuthentication
    {
        public bool AttemptLogin(string username, string password)
        {
            
            return FactoryDataLayer.getDetails().isExists(username, password);
        }
        
    }
}
