using System;
using DataAccessLogic;
namespace BusinessLogic
{
    class User : ILogin
    {
        private string m_username;
        private string m_password;
        public User(string username, string password)
        {
            this.m_username = username;
            this.m_password = password;
        }
        public bool AttemptLogin()
        {
            IGetDetails detailsObject = null;
            detailsObject = GetDetails.getDetails(m_username, m_password);
            if (detailsObject.isExists())
            {
                return true;
            }
            return false;
        }
        public string getUserName()
        {
            IGetDetails objectType = GetDetails.getDetails(m_username, m_password);
            string value = objectType.getStudentName();
            return value;
        }

        public string getUserId()
        {
            IGetDetails objectType = GetDetails.getDetails(m_username, m_password);
            string value = objectType.getStudentId();
            return value;
        }
    }
}
