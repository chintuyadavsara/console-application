using DomainModal;

namespace BusinessLogic
{
    public interface IStudentBussiness
    {
       
        Student GetStudentDetails(string studentId);
        
    }
}