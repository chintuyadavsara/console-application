using DomainModal;
using DataAccessLogic;
using System;

namespace BusinessLogic
{
    internal class StudentBussiness : IStudentBussiness
    {
       
       
        public Student GetStudentDetails(string username)
        {
            return DataAccessLogic.FactoryDataLayer.GetStudentRepo().GetStudentDetails(username);
        }
    }
}