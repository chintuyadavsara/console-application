﻿using System;
using BusinessLogic;
namespace FactoryMethod
{
    class Program
    {
        static void Main(string[] args)
        {

            int attemptsCount = 4;
            int flagError = 1;
            Console.WriteLine("1.Login\n2.Register");
            int option = int.Parse(Console.ReadLine());
            
            switch(option)
            {
                case 1:
                    while (attemptsCount > 0 && flagError != 0)
                    {
                        attemptsCount = attemptsCount - 1;
                        Console.Write("Email:  ");
                        string userEmail = Console.ReadLine();
                        Console.Write("Password:  ");
                        string password = Console.ReadLine();
                        ILogin loginType = Login.attemptLogin(userEmail, password);
                        if (loginType.AttemptLogin())
                        {
                            flagError = 0;
                            Console.WriteLine("Welcome {0}", userEmail);
                            Console.WriteLine("Name: " + loginType.getUserName());
                            Console.WriteLine("Id: " + loginType.getUserId());
                            break;
                        }
                        else
                        {
                            flagError = 1;
                            IError errorType = Error.ShowError();
                            errorType.showError();
                            Console.WriteLine("Sorry, You have {0} attempts remaining\n", attemptsCount);
                        }
                    }
                    break;
                case 2:
                    Console.Write("Email:  ");
                    string email = Console.ReadLine();
                    Console.Write("Password:  ");
                    string registerPassword = Console.ReadLine();
                    Console.Write("Name:  ");
                    string name = Console.ReadLine();
                    Console.Write("Id:  ");
                    string id = Console.ReadLine();
                    Console.WriteLine("Please wait for 1 day, You are on Queue |- - - -|");
                    break;
                default:
                    Console.WriteLine("404 Error - Page not found :(");
                    break;
            }
        }
    }
}
