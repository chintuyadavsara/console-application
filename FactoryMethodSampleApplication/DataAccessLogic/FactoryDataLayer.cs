using System;
namespace DataAccessLogic
{
    public static class FactoryDataLayer
    {
        public static IGetDetails getDetails()
        {
            return  new GetDetails();
        }
        public static IStudentRepo GetStudentRepo()
        {
            return new StudentRepo();
        }
    }
}