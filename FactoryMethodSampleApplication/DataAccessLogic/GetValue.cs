using System;
using System.Collections.Generic;
namespace DataAccessLogic
{
    public class GetValue : IGetDetails
    {
        public List<string[]> m_map_data = new List<string[]>();
        
        private string m_username = string.Empty;
        private string m_password = string.Empty;
        private string m_name = string.Empty;
        private string m_id = string.Empty;
        public GetValue(string username, string password)
        {
            string[] dataBase = { "test@gmail.com", "Sample Test", "2205", "password" };
            m_map_data.Add(dataBase);
            string[] dataBase1 = { "test1@gmail.com", "Sample Test 1", "2204", "password1" };
            m_map_data.Add(dataBase1);
            this.m_username = username;
            this.m_password = password;
        }
        public GetValue(string username, string password, string name, string id)
        {
            string[] dataBase = { username, name, id, password };
            m_map_data.Add(dataBase);
            
            this.m_username = username;
            this.m_password = password;
            this.m_name = name;
            this.m_id = id;
        }
        public bool isExists()
        {
            int listLenght = m_map_data.Count;
            for(int index = 0; index < listLenght ; index++)
            {
                if((m_map_data[index][0].Equals(m_username)) && (m_map_data[index][3].Equals(m_password)))
                {
                    return true;
                }
            }
            return false;
        }
        public string getStudentName()
        {
            int listLenght = m_map_data.Count;
            for(int index = 0; index < listLenght ; index++)
            {
                if((m_map_data[index][0].Equals(m_username)) && (m_map_data[index][3].Equals(m_password)))
                {
                    return m_map_data[index][1];
                }
            }
            return "NAN";
        }
        public string getStudentId()
        {
            int listLenght = m_map_data.Count;
            for(int index = 0; index < listLenght ; index++)
            {
                if((m_map_data[index][0].Equals(m_username)) && (m_map_data[index][3].Equals(m_password)))
                {
                    return m_map_data[index][2];
                }
            }
            return "NAN";
            
        }
        
    }
}