using DomainModal;
using DataModal;
using System;
using System.Linq;

namespace DataAccessLogic
{
    internal class StudentRepo : IStudentRepo
    {
        public Student GetStudentDetails(string username)
        {
            var result = DataSource.StudentList.Where(m => m.EmailId == username).FirstOrDefault();
            //Console.WriteLine("Type Before Casting-->"+ result.GetType());
            Mapper<TblStudent, Student> map = new Mapper<TblStudent, Student>();
            return map.Convert(result);
        }
    }
}