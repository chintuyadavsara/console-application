using DomainModal;
using DataModal;
using System.Linq;
using System;
using System.Reflection;

namespace DataAccessLogic
{
    public class Mapper<T, U> where U : new()
    {
        public U Convert(T input)
        {
            U newCastType = new U();
            var fromObjectProperties = input.GetType().GetProperties();
            var toObjectProperties = newCastType.GetType().GetProperties();

            foreach (var parentProperty in fromObjectProperties)
            {

                foreach (var childProperty in toObjectProperties)
                {
                    if (parentProperty.Name == childProperty.Name)
                    {
                        childProperty.SetValue(newCastType, parentProperty.GetValue(input));
                    }
                }
            }
            /*var fromObjectProperties = input.GetType().GetProperties();
            foreach (var parentProperty in fromObjectProperties)
            {
                
            }*/
            return newCastType;

        }

    }
}