using DomainModal;
using DataModal;
namespace DataAccessLogic
{
    public interface IStudentRepo
    {
        Student GetStudentDetails(string username);
    }
}