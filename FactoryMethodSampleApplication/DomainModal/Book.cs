using System;

namespace DomainModal
{
    public class Book
    {
        private string bookName = string.Empty;
        private string bookPublishYear = string.Empty;

        public Book(string bookName, string bookPublishYear)
        {
            this.bookName = bookName;
            this.bookPublishYear = bookPublishYear;
        }
        public string GetBookName()
        {
            return bookName;
        }
        public string GetBookPublishYear()
        {
            return bookPublishYear;
        }
    }
}
