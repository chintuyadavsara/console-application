﻿using System;

namespace DomainModal
{
    public class Student
    {

        public string EmailId { get; set; }
        public string Password { get; set; }
        public string StudentName { get; set; }
        public string StudentId { get; set; }
        public string PhoneNumber { get; set; }
        public Book GetBook { get; set; }

    }
}
