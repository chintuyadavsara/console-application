using System;

namespace DataModal
{
    public class TblBook
    {
        private string bookName = string.Empty;
        private string bookPublishYear = string.Empty;

        public TblBook(string bookName, string bookPublishYear)
        {
            this.bookName = bookName;
            this.bookPublishYear = bookPublishYear;
        }
        
        public string GetBookName()
        {
            return bookName;
        }
        public string GetBookPublishYear()
        {
            return bookPublishYear;
        }
    }
}
