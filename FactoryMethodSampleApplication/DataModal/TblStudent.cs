﻿using System;

namespace DataModal
{
    public class TblStudent
    {

        public string EmailId { get; set; }
        public string Password { get; set; }
        public string StudentName { get; set; }
        public string StudentId { get; set; }
        public string PhoneNumber { get; set; }
        public TblBook GetBook { get; set; }

    }
}
