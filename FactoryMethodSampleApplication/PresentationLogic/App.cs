using System;
using BusinessLogic;
namespace PresentationLogic
{
    public class App
    {
        public static void Login()
        {
            Console.Write("Email:  ");
            string username = Console.ReadLine();
            Console.Write("Password:  ");
            string password = Console.ReadLine();
            bool status = FactoryBusiness.Authenticate().AttemptLogin(username, password);
            if (status)
            {
                Console.WriteLine("Name: " + FactoryBusiness.GetStudentRepo().GetStudentDetails(username).StudentName);
                //Console.WriteLine("Id: " + FactoryBusiness.GetStudentRepo().GetStudentDetails(username).StudentId);
               // Console.WriteLine("Book Name: " + FactoryBusiness.GetStudentRepo().GetStudentDetails(username).GetBook.GetBookName());
               // Console.WriteLine("Published Year: " + FactoryBusiness.GetStudentRepo().GetStudentDetails(username).GetBook.GetBookPublishYear());
            }
            else
            {
                IError errorType = Error.ShowError();
                errorType.showError();
            }
        }
    }
}